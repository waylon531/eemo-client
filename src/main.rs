//
//    eemo-client, a client for the eemo electrical-engineering mmo
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#[macro_use] extern crate conrod;
extern crate ttf_noto_sans;
extern crate libeemo;
extern crate rustls;
extern crate webpki_roots;
extern crate image;

use conrod::text::FontCollection;
use conrod::{widget, Colorable, Positionable, Widget,Sizeable,Labelable};
use conrod::backend::glium::glium;
use conrod::backend::glium::glium::{DisplayBuild, Surface};
use conrod::position::{Relative,Align};

use std::sync::mpsc;
use std::thread;

mod listener;
mod state;
mod button;
use state::{State,Images};

use libeemo::{ClientToServerMessage,Direction,Gate,ObjectData,Position};
fn main() {
    //spawn network listener thread
    //gui communicates over channel
    let (tx, receiver) = mpsc::channel();
    let (sender, rx) = mpsc::channel();
    thread::spawn(move || listener::run(tx,rx,"eemo.firechicken.net".to_owned(),44938));

    sender.send(ClientToServerMessage::GetName).unwrap();


    
    const WIDTH: u32 = 1024;
    const HEIGHT: u32 = 768;

    // Build the window.
    let display = glium::glutin::WindowBuilder::new()
        .with_vsync()
        .with_dimensions(WIDTH, HEIGHT)
        .with_title("eemo-client")
        .with_multisampling(4)
        .build_glium()
        .unwrap();

    //Load images
    let images = Images::new(&display);

    // construct our `Ui`.
    let mut ui = conrod::UiBuilder::new([WIDTH as f64, HEIGHT as f64]).build();

    let mut state = State::new();


    // Generate the widget identifiers.
    widget_ids!(struct Ids { 
        chat,
        chat_box,
        chat_entry,
        username,
        password,
        login_button,
        register_button,
        world_matrix,
        right_button,
        left_button,
        object_box,
        object_preview
    });
    let ids = Ids::new(ui.widget_id_generator());

    let _ = ui.fonts.insert(FontCollection::from_bytes(ttf_noto_sans::REGULAR)
    .into_font()
    .expect("failed to into_font"));
    // Add a `Font` to the `Ui`'s `font::Map` from file.
    //const FONT_PATH: &'static str =
    //    concat!(env!("CARGO_MANIFEST_DIR"), "/NotoSans-Regular.ttf");
    //ui.fonts.insert_from_file(FONT_PATH).unwrap();

    // A type used for converting `conrod::render::Primitives` into `Command`s that can be used
    // for drawing to the glium `Surface`.
    let mut renderer = conrod::backend::glium::Renderer::new(&display).unwrap();

    // Poll events from the window.
    let mut last_update = std::time::Instant::now();
    let mut loop_counter = 0;
    'main: loop {

        if loop_counter >= 10 {
            loop_counter = 0;
            sender.send(ClientToServerMessage::GetWorld).unwrap();
        } else {
            loop_counter+=1;
        }
        // We don't want to loop any faster than 60 FPS, so wait until it has been at least
        // 16ms since the last yield.
        let sixteen_ms = std::time::Duration::from_millis(16);
        let duration_since_last_update = std::time::Instant::now().duration_since(last_update);
        if duration_since_last_update < sixteen_ms {
            std::thread::sleep(sixteen_ms - duration_since_last_update);
        }
        //Process events from network thread
        for message in receiver.try_iter() {
            state.process_message(message);
            ui.needs_redraw();
        }

        // Collect all pending events.
        let events: Vec<_> = display.poll_events().collect();


        // If there are no events and the `Ui` does not need updating, wait for the next event.
        //if events.is_empty() && !ui_needs_update {
        //    events.extend(display.wait_events().next());
        //}

        // Reset the needs_update flag and time this update.
        last_update = std::time::Instant::now();

        // Handle all events.
        for event in events {

            // Use the `winit` backend feature to convert the winit event to a conrod one.
            if let Some(event) = conrod::backend::winit::convert(event.clone(), &display) {
                ui.handle_event(event);
            }

            use glium::glutin::ElementState::Pressed;
            match event {
                glium::glutin::Event::Closed =>
                    break 'main,
                glium::glutin::Event::KeyboardInput(Pressed, _, Some(glium::glutin::VirtualKeyCode::Left)) => {
                    sender.send(ClientToServerMessage::Move(state.move_player(-1,0))).unwrap();
                    ui.needs_redraw();
                },
                glium::glutin::Event::KeyboardInput(Pressed, _, Some(glium::glutin::VirtualKeyCode::Right)) => {
                    sender.send(ClientToServerMessage::Move(state.move_player(1,0))).unwrap();
                    ui.needs_redraw();
                },
                glium::glutin::Event::KeyboardInput(Pressed, _, Some(glium::glutin::VirtualKeyCode::Up)) => {
                    sender.send(ClientToServerMessage::Move(state.move_player(0,1))).unwrap();
                    ui.needs_redraw();
                },
                glium::glutin::Event::KeyboardInput(Pressed, _, Some(glium::glutin::VirtualKeyCode::Down)) => {
                    sender.send(ClientToServerMessage::Move(state.move_player(0,-1))).unwrap();
                    ui.needs_redraw();
                },
                _ => {},
            }
        }


        let message_string = state.get_chat_messages();

        // Instantiate all widgets in the GUI.
        {
            let ui = &mut ui.set_widgets();


            //Box to allow scrolling of text
            widget::Rectangle::outline([600.0,200.0])
                .color(conrod::color::WHITE)
                .scroll_kids()
                .x_y_position_relative_to(ids.chat_entry,
                                          Relative::Align(Align::Start),
                                          Relative::Scalar(140.0))
                .set(ids.chat_box, ui);
            //Received chat messages
            widget::Text::new(message_string.as_str())
                .color(conrod::color::WHITE)
                .font_size(14)
                .parent(ids.chat_box)
                .padded_kid_area_w_of(ids.chat_box,5.0)
                .wrap_by_word()
                .left_justify()
                .align_bottom()
                .set(ids.chat, ui);
            for event in widget::TextBox::new(state.get_chat_message().as_str())
                //.x_y(10.0,700.0)
                //.down(80f64)
                //.x_y(10.0,0.0)
                .bottom_left_with_margin_on(ui.window,15.0)
                .w_h(512.0, 40.0)
                .color(conrod::color::GREEN)
                .font_size(14)
                .set(ids.chat_entry, ui) 
                {
                    use conrod::widget::text_box::Event;
                    match event {
                        Event::Update(s) => state.update_chat_message(s),
                        Event::Enter => {
                            sender.send(ClientToServerMessage::ChatMessage(state.get_chat_message())).unwrap();
                        }
                    }
                }
            for event in widget::TextBox::new(state.get_string("username").as_str())
                //.x_y(10.0,700.0)
                //.down(80f64)
                //.x_y(10.0,0.0)
                .left_from(ids.password,10.0)
                .w_h(100.0, 40.0)
                .color(conrod::color::WHITE)
                .font_size(14)
                .set(ids.username, ui) 
                {
                    use conrod::widget::text_box::Event;
                    match event {
                        Event::Update(s) => state.update_string("username",s),
                        Event::Enter => {
                            sender.send(ClientToServerMessage::Auth(state.get_string("username"),state.get_string("password"))).unwrap();
                            //TODO: The position should be checked once the user
                            //has successfully logged in, not on every attempt
                            sender.send(ClientToServerMessage::GetPosition).unwrap();
                            sender.send(ClientToServerMessage::GetName).unwrap();
                        } 
                    }
                }
            for event in widget::TextBox::new(state.get_string("password").chars().map(|_| "*").collect::<String>().as_str())
                .x_y_relative_to(ids.register_button,20.0,50.0)
                .w_h(100.0, 40.0)
                .color(conrod::color::WHITE)
                .font_size(14)
                .set(ids.password, ui) 
                {
                    use conrod::widget::text_box::Event;
                    match event {
                        Event::Update(s) => 
                        {
                            let mut old_string = state.get_string("password");
                            let new_string: String =  if s.len() > old_string.len() {
                                    old_string + s.chars().filter(|&c| c != '*').collect::<String>().as_str()
                                } else if s.len() < old_string.len() {
                                    old_string.truncate(s.len());
                                    old_string
                                } else {
                                    old_string
                                };
                            state.update_string("password",new_string);
                        },
                        Event::Enter => {
                            sender.send(ClientToServerMessage::Auth(state.get_string("username"),state.get_string("password"))).unwrap();
                            sender.send(ClientToServerMessage::GetPosition).unwrap();
                            sender.send(ClientToServerMessage::GetName).unwrap();
                        } 
                    }
                }
            for _click in widget::Button::new()
                .bottom_right_with_margins_on(ui.window,10.0,50.0)
                .w_h(60.0, 40.0)
                .color(conrod::color::YELLOW)
                .label("register")
                .label_font_size(16)
                .enabled(true)
                .set(ids.register_button, ui) 
                {
                    sender.send(ClientToServerMessage::Register(state.get_string("username"),state.get_string("password"))).unwrap();
                }
            for _click in widget::Button::new()
                .left_from(ids.register_button,10.0)
                .w_h(60.0, 40.0)
                .color(conrod::color::YELLOW)
                .label("login")
                .label_font_size(16)
                .enabled(true)
                .set(ids.login_button, ui) 
                {
                    sender.send(ClientToServerMessage::Auth(state.get_string("username"),state.get_string("password"))).unwrap();
                    sender.send(ClientToServerMessage::GetPosition).unwrap();
                    sender.send(ClientToServerMessage::GetName).unwrap();
                }

            let (cols,rows) = (21,21);
            let mut elements = widget::Matrix::new(cols, rows)
                 .w_h(450.0, 450.0)
                 .top_left_with_margin_on(ui.window,20.0)
                 .set(ids.world_matrix,ui);

            while let Some(elem) = elements.next(ui) {
                let (x, y) = (
                    (elem.col as i64) - (cols as i64/2), 
                    ((elem.row as i64) - (cols as i64/2)) * -1
                    );
                let image_widget;
                if (x,y) == (0,0) {
                    image_widget = button::Button::image(images.player);
                } else if 
                    state.get_player_at(x,y).is_some() &&
                    state.get_player_at(x,y) != Some(&state.get_name()) { 
                    let name = state.get_player_at(x,y).unwrap();
                    image_widget = button::Button::image(images.player)
                        .label(name.as_str())
                        .label_font_size(10)

                        
				}
                else if let Some(object) = state.get_object_at(x,y) {
                    let orientation = object.get_orientation();
                    let image = match object.get_data() {
                        ObjectData::Gate(Gate::And) => &images.and,
                        ObjectData::Gate(Gate::Or) => &images.or,
                        ObjectData::Gate(Gate::Xor) => &images.xor,
                        ObjectData::Lever(true) => &images.lever.on,
                        ObjectData::Lever(false) => &images.lever.off,
                        ObjectData::Light(true) => &images.light.on,
                        ObjectData::Light(false) => &images.light.off,
                    };
                    let rotated_image = match orientation {
                        Direction::North => image.up,
                        Direction::East => image.right,
                        Direction::South => image.down,
                        Direction::West => image.left
                    };
                    image_widget = button::Button::image(rotated_image);
                } else {
                    image_widget = button::Button::image(images.background);
                }
                let click = elem.set(image_widget,ui);
                {
                    //TODO: Figure out way to send a value instead of 0
                    use button::Interaction;
                    match click {
                        (_, 0) => {}, //No clicks, so no action
                        (Interaction::RightPress,_num) => sender.send(ClientToServerMessage::Activate(Position(x,y)+ state.get_position(),0)).unwrap(),
                        (Interaction::Press,_num) => match state.get_entered_object() {
                            Some(object) => sender.send(ClientToServerMessage::ObjectAdd(object,Position(x,y) + state.get_position())).unwrap(),
                            None => {}
                        },
                        _ => {}
                    }
                }

            }

            for _click in widget::Button::new()
                .mid_right_with_margin(10.0)
                .w_h(60.0, 40.0)
                .color(conrod::color::YELLOW)
                .label("right")
                .label_font_size(16)
                .enabled(true)
                .set(ids.right_button, ui) 
                {
                    state.rotate_right();
                }
            for _click in widget::Button::new()
                .left_from(ids.right_button,10.0)
                .w_h(60.0, 40.0)
                .color(conrod::color::YELLOW)
                .label("left")
                .label_font_size(16)
                .enabled(true)
                .set(ids.left_button, ui) 
                {
                    state.rotate_left();
                }
            for event in widget::TextBox::new(state.get_string("object_entry").as_str())
                //.x_y(10.0,700.0)
                //.down(80f64)
                //.x_y(10.0,0.0)
                .down_from(ids.left_button,10.0)
                .w_h(100.0, 40.0)
                .color(conrod::color::WHITE)
                .font_size(14)
                .set(ids.object_box, ui) 
                {
                    use conrod::widget::text_box::Event;
                    match event {
                        Event::Update(s) => state.update_string("object_entry",s),
                        Event::Enter => {
                            //Image is updated after every keystroke,
                            //no need to do anythin special here
                        } 
                    }
                }
            match state.get_entered_object() { 
                Some(object) => {
                    let orientation = object.get_orientation();
                    let image = match object.get_data() {
                        ObjectData::Gate(Gate::And) => &images.and,
                        ObjectData::Gate(Gate::Or) => &images.or,
                        ObjectData::Gate(Gate::Xor) => &images.xor,
                        ObjectData::Lever(true) => &images.lever.on,
                        ObjectData::Lever(false) => &images.lever.off,
                        ObjectData::Light(true) => &images.light.on,
                        ObjectData::Light(false) => &images.light.off,
                    };
                    let rotated_image = match orientation {
                        Direction::North => image.up,
                        Direction::East => image.right,
                        Direction::South => image.down,
                        Direction::West => image.left
                    };
                    button::Button::image(rotated_image)
                        .down_from(ids.object_box,10.0)
                        .w_h(64.0,64.0)
                        .set(ids.object_preview,ui);
                },
                None => {}
            }
        }

        // Render the `Ui` and then display it on the screen.
        if let Some(primitives) = ui.draw_if_changed()
        {
            renderer.fill(&display, primitives, &images.map);
            let mut target = display.draw();
            target.clear_color(0.0, 0.0, 0.0, 1.0);
            renderer.draw(&display, &mut target, &images.map).unwrap();
            target.finish().unwrap();
        }
    }
}

