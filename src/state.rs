//
//    eemo-client, a client for the eemo electrical-engineering mmo
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

use libeemo::{Position,AuthError,SendWorld,ServerToClientMessage,Object,Direction,ObjectData,Gate};
use std::collections::HashMap;
use std::fmt::Write;
use std::path::Path;
use std::fmt;
use image;
use image::imageops;
use conrod;
use conrod::glium;
pub struct State {
    chat_messages: Vec<(String,String)>,
    text_strings: HashMap<&'static str,String>, //A map of strings from textboxes
    world: SendWorld,
    position: Position,
    source: String,
    auth_error: Option<Result<(),AuthError>>,
    name: String,
    direction: Direction
}
impl State {
    pub fn new() -> State {
        State {  
            chat_messages: Vec::new(),
            text_strings: HashMap::new(),
            world: SendWorld {
                players: HashMap::new(),
                objects: HashMap::new()
            },
            position: Position(0,0),
            source: "".to_owned(),
            auth_error: None,
            name: "".to_owned(),
            direction: Direction::North,

        }
    }
    pub fn process_message(&mut self, message: ServerToClientMessage) {
        match message {
            ServerToClientMessage::Game(world) => self.world = world,
            ServerToClientMessage::YourPosition(pos) => self.position = pos,
            ServerToClientMessage::ChatMessage(message,user) => self.chat_messages.push((message,user)),
            ServerToClientMessage::Source(source) => self.source = source,
            ServerToClientMessage::AuthResponse(result) => self.auth_error = Some(result),
            ServerToClientMessage::Name(name) => self.name = name
        }
    }
    pub fn get_chat_messages(&self) -> String {
        let mut writable = String::new();
        for &(ref message,ref user) in self.chat_messages.iter(){
            writeln!(writable,"{}: {}",user,message).unwrap();
        }
        writable
        
    }
    pub fn update_string(&mut self, key: &'static str, data: String) {
        self.text_strings.insert(key,data);
    }
    pub fn get_string(&self, key: &'static str) -> String {
        self.text_strings.get(key).unwrap_or(&"".to_owned()).clone()
    }
    pub fn update_chat_message(&mut self,s: String) {
        self.text_strings.insert("chat_message",s);
    }
    pub fn get_chat_message(&self) -> String {
        self.text_strings.get("chat_message").unwrap_or(&"".to_owned()).clone()
    }
    pub fn move_player(&mut self, x: i64, y: i64) -> Position {
        self.position.0 += x;
        self.position.1 += y;
        self.position
    }
    pub fn get_object_at<'a>(&'a self,x: i64, y: i64) -> Option<&'a Object> {
        self.world.objects.get(&Position(x+self.position.0,y+self.position.1))
    }
    pub fn get_player_at<'a>(&'a self,x: i64, y: i64) -> Option<&'a String> {
        match self.world.players.iter().filter(|&(_k,v)| v == &Position(x+self.position.0,y+self.position.1)).next() {
            Some((k,_v)) => Some(k),
            None => None
        }
    }
    pub fn get_name(&self) -> String {
        self.name.clone()
    }
    pub fn get_position(&self) -> Position {
        self.position
    }
    pub fn rotate_left(&mut self) {
        self.direction = self.direction + Direction::West;
    }
    pub fn rotate_right(&mut self) {
        self.direction = self.direction + Direction::East;
    }
    pub fn get_entered_object(&self) -> Option<Object> {
        Some(Object {
            orientation: self.direction,
            object_data: match self.get_string("object_entry").to_lowercase().as_str() {
                "and" => ObjectData::Gate(Gate::And),
                "or" => ObjectData::Gate(Gate::Or),
                "xor" => ObjectData::Gate(Gate::Xor),
                "lever" => ObjectData::Lever(false),
                "light" => ObjectData::Light(false),
                _ => return None
            }
        })
    }
}
pub struct OnOffImage {
    pub on: ImageRot,
    pub off: ImageRot
}
impl OnOffImage {
    pub fn new<P: AsRef<Path> + fmt::Display + Clone> (
        display: &glium::Display,
        mut map: &mut conrod::image::Map<glium::texture::Texture2d>,
        off_path: P,
        on_path: P,
    ) -> OnOffImage {

        OnOffImage {
            on: ImageRot::new(display,map,on_path),
            off: ImageRot::new(display,map,off_path)
        }
    }
}
pub struct ImageRot {
    pub up: conrod::image::Id,
    pub down: conrod::image::Id,
    pub left: conrod::image::Id,
    pub right: conrod::image::Id,
}
impl ImageRot {
    pub fn new<P: AsRef<Path> + fmt::Display + Clone> (
        display: &glium::Display,
        mut map: &mut conrod::image::Map<glium::texture::Texture2d>,
        path: P
    ) -> ImageRot {

        let image = match image::open(path.clone()) {
            Ok(v) => v,
            Err(e) => panic!("Could not load image at path {}, with error {}",path,e)
        }.to_rgba();
        ImageRot {
            down: map_insert(display,map,imageops::rotate180(&image)),
            left: map_insert(display,map,imageops::rotate270(&image)),
            right: map_insert(display,map,imageops::rotate90(&image)),
            up: map_insert(display,map,image),
        }
            
    }
}
pub struct Images {
    pub map: conrod::image::Map<glium::texture::Texture2d>,
    pub xor: ImageRot,
    pub and:  ImageRot,
    pub or: ImageRot,
    pub light: OnOffImage,
    pub lever: OnOffImage,
    pub player: conrod::image::Id,    
    pub background: conrod::image::Id,
}
impl Images {
    pub fn new(display: &glium::Display) -> Images {
        let mut map = conrod::image::Map::new();
        Images {
            xor: ImageRot::new(display,&mut map,"img/Gate_XOR.png"),
            or: ImageRot::new(display,&mut map,"img/Gate_OR.png"),
            and: ImageRot::new(display,&mut map,"img/Gate_AND.png"),
            light: OnOffImage::new(display,&mut map,"img/Light_off.png","img/Light_on.png"),
            lever: OnOffImage::new(display,&mut map,"img/Lever_off.png","img/Lever_on.png"),
            player: load_image(display,&mut map,"img/Player.png"),
            background: load_image(display,&mut map,"img/Background.png"),
            map: map
        }
    }
}
//Loads a single image from a file
fn load_image<P: AsRef<Path>>(display: &glium::Display,map: &mut conrod::image::Map<glium::texture::Texture2d>, path: P) -> conrod::image::Id {
    map_insert(display,map, image::open(path).expect("Failed to find gate image").to_rgba())
}

//Inserts an image into the image map, doing some necessary conversions
fn map_insert(display: &glium::Display,mut map: &mut conrod::image::Map<glium::texture::Texture2d>, image: image::RgbaImage) -> conrod::image::Id{
    let image_dimensions = image.dimensions();
    let raw_image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(), image_dimensions);
    let texture = glium::texture::Texture2d::new(display, raw_image).unwrap();
    map.insert(texture)
}
