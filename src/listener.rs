//
//    eemo-client, a client for the eemo electrical-engineering mmo
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

use std::io::{Read,ErrorKind};
use std::net::TcpStream;
use std::thread;
use std::time::Duration;
use std::sync::mpsc::{Sender,Receiver};
use std::sync::Arc;

use rustls;
use rustls::Session;

use webpki_roots;

use libeemo::{ServerToClientMessage,ClientToServerMessage};
use libeemo;

pub fn run(tx: Sender<ServerToClientMessage>,rx: Receiver<ClientToServerMessage>, hostname: String,port: u16) {
    let mut config = rustls::ClientConfig::new();
    config.root_store.add_trust_anchors(&webpki_roots::ROOTS);
    let rc_config = Arc::new(config);
    let mut client = rustls::ClientSession::new(&rc_config, hostname.as_str());

    let mut stream = TcpStream::connect((hostname.as_str(),port)).expect("Couldn't connect to server");
    //Set a timeout so this thread won't get hung up on reads
    stream.set_read_timeout(Some(Duration::from_millis(1))).expect("Failed to set read timeout");

    let mut buffer = Vec::new();
    'listener: loop {
        //Send all messages in queue
        'sendmessages: loop {
            match rx.try_recv() {
                Ok(message) => {
                    println!("SENT: {:?}",message);
                    message.write_out(&mut client).expect("Unable to write message to TLS session");
                },
                //Maybe TODO: handle disconnected stream
                Err(_) => break
            }
        }
        if client.wants_write() {
            //TODO: send stuff in queue
            client.write_tls(&mut stream).unwrap();
        }
        if client.wants_read() {
            match client.read_tls(&mut stream) {
                Ok(0) => {
                    //TODO: I really don't want to panic
                    panic!("Server closed the connection");
                },
                Ok(_) => {
                    //println!("BUFFER: {:?}",buffer);
                    client.process_new_packets().unwrap();
                    client.read_to_end(&mut buffer).expect("Failed to read into buffer");
                    //This is triggered when handshake stuff is happening
                    if buffer.len() == 0 {
                        continue 'listener;
                    }
                    let message = match ServerToClientMessage::from_vec(&mut buffer) {
                        Ok(m) => m,
                        Err(e) => {
                            //If there's some invalid data then toss it
                            //buffer.clear();
                            match e {
                                libeemo::decode::Error::InvalidMarkerRead(_) => continue 'listener,
                                _ => {
                                    buffer.clear(); 
                                    continue 'listener;
                                }
                            }
                        }
                    };
                    println!("GOT MESSAGE: {:?}",message);
                    //
                    //TODECIDE
                    //Do I want to have the gui state thread handle this stuff?
                    //It'd probably be simplest
                    //Other option would be locks + shared state
                    //
                    //Send message back to gui
                    tx.send(message).unwrap();

                },
                Err(e) => {
				    match e.kind() {
                        //If the error is that it's timed out
                        //then eveything is fine and we can NOP
                        ErrorKind::WouldBlock => {
                            thread::sleep(Duration::from_millis(1));
                        },	
                        ErrorKind::TimedOut => {
                            thread::sleep(Duration::from_millis(1));
                        },	
                        //TODO: get rid of panics
                        _ => panic!("Error reading from TCP stream into TLS session: {}",e)
                    }
                }	
            }

        } else {
            thread::sleep(Duration::from_millis(1));
        }
    }
}
